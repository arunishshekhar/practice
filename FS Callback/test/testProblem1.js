const { createAndDeleteJSON,directoryCreator, creatingJSON,directoryDeleator} = require("../problem1")

let dict = {
    "one": [15, 4.5],
    "two": [34, 3.3],
    "three": [67, 5.0],
    "four": [32, 4.1]
};

let dictstring = JSON.stringify(dict);

createAndDeleteJSON(directoryCreator());
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(creatingJSON(undefined, dictstring));
createAndDeleteJSON(directoryDeleator());

//setTimeout(()=>{createAndDeleteJSONFile.createAndDeleteJSON(dictstring)},1000);