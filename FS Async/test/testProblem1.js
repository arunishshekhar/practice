
const { directoryChecker, jsonCreator, directoryDeleator } = require('../problem1')

let dict = {
    "one": [15, 4.5],
    "two": [34, 3.3],
    "three": [67, 5.0],
    "four": [32, 4.1]
};

let dictstring = JSON.stringify(dict);

async function problemSolver() {
    try {
        const directoryName = await directoryChecker("DataStorage");
        await jsonCreator(directoryName, dictstring);
        await jsonCreator(directoryName, dictstring);
        await jsonCreator(directoryName, dictstring);
        await jsonCreator(directoryName, dictstring);
        await jsonCreator(directoryName, dictstring);
        await directoryDeleator(directoryName);

    } catch (err) {
        console.error(err)
    }
}

problemSolver();
