const { readingfile, writingfile, deleteFiles, appendingfile } = require('../problem2')

async function problemSolver() {
    try {
        const mainFileData = await readingfile("lipsum.txt");
        const upperCaseFileName = await writingfile("uppercase.txt", mainFileData.toUpperCase());
        const appendedUpperCaseFileName = await appendingfile("filename.txt", upperCaseFileName);
        const upperCaseFileData = await readingfile(appendedUpperCaseFileName);
        const lowerCaseFileName = await writingfile('lowercase.txt', upperCaseFileData.toLowerCase().split(".").map((eachLine) => { return eachLine.trim() }).join('\n').trim());
        const appendedLowerCaseFileName = await appendingfile("filename.txt", lowerCaseFileName);
        const lowerCaseFileData = await readingfile(appendedLowerCaseFileName);
        const sortedLowerCaseFileName = await writingfile('sortedlower.txt', lowerCaseFileData.split("\n").sort().join("\n"));
        await appendingfile("filename.txt", sortedLowerCaseFileName);
        const newFilesCreatedName = await readingfile("filename.txt");
        await deleteFiles(newFilesCreatedName);
    } catch (err) {
        console.log(err);
    }
}

problemSolver();