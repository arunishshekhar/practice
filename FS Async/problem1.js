/*
    Problem 1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');
const path = require('path');

const workingDir = path.join(__dirname);

function directoryChecker(directoryName) {
    return new Promise((resolve) => {
        if (fs.existsSync(path.join(workingDir, directoryName))) {
            console.log("Directory Exists");
            resolve(directoryName);
        }
        else {
            console.log("Directory Creating");
            const returnedDirectoryName = directoryCreator(directoryName)
            resolve(returnedDirectoryName);
        }
    })
}


async function directoryCreator(directoryName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.join(workingDir, directoryName), { recursive: true }, (err) => {
            if (err) {
                reject("Error Wile Creating Directory");
            }
            else {
                console.log("Directory Created");
                resolve(directoryName);
            }
        })
    })
}

function fileNameCreator() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < 6; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    text = text + ".json";
    return text;
}

function jsonCreator(directoryName, dataToWrite) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(workingDir, directoryName, fileName = fileNameCreator()), dataToWrite, (err) => {
            if (err) {
                reject("Error while writing file");
            }
            else {
                console.log("File created successfully ", fileName);
                resolve(directoryName);
            }
        })
    })
}

const directoryDeleator = (directoryName) => {
    return new Promise(() => {
        fs.readdir(path.join(workingDir, directoryName), (err, files) => {
            if (err)
                console.log(err);
            else {
                console.log("\nCurrent directory filenames:");
                files.forEach(file => {
                    deleteJSON(directoryName, file);
                })
            }
        })
    })
};
const deleteJSON = (directoryName, fileName) => {
    fs.unlink(path.join(workingDir, directoryName, fileName), (err) => {
        if (err) {
            throw err;
        }
        else {
            console.log("Deleted ", path.join(workingDir, fileName).toString());
        }
    });
};


module.exports = { directoryChecker, jsonCreator, directoryDeleator }
