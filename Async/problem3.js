/* Q3. Write a function to get all the users information from the users API url. 
Find all the users with name "Nicholas". 
Get all the to-dos for those user ids. */

/* Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos */

const fs = require('fs');
const axios = require('axios');


function fetchingAPIData(url) {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then(response => resolve(response.data))
            .catch(err => reject("Failed to fetch " + url + ": " + err));
    })

}

async function solution() {
    const usersData = await fetchingAPIData("https://jsonplaceholder.typicode.com/users");
    const detailOfUsers = usersData.filter(user => {
        if (user['name'].includes('Nicholas'))
            return user;
    })
    let idOfUsers = detailOfUsers.map(user => user['id'])
    const todosData = await fetchingAPIData("https://jsonplaceholder.typicode.com/todos");
    let returningArray = [];
    idOfUsers.forEach(user => { 
        let tempData = todosData.filter(todo => {
            return user === todo['userId'];
        })
        //console.log(tempData);
        returningArray.push(tempData);
    })
    //console.log(returningArray);
    return returningArray;
}

solution().then((data) => {console.log(data);})