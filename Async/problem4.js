/* Q4. Write a function that takes any number of arguments ..and makes api call for each of them to fetch the results.
        A. use Promises.all
        B. use only 1 api call to get all the results.
Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos
Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913&id=2399
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392&userId=213921398
 */

const fs = require('fs');
const axios = require('axios');


function fetchingAPIData(url, extraData) {
    return new Promise((resolve, reject) => {
        axios.get(url + extraData)
            .then(response => resolve(response.data))
            .catch(err => reject("Failed to fetch " + url + ": " + err));
    })

}

async function urlAppend(userIDs, isUserDetail = true) {

    return new Promise((resolve, reject) => {
        const urlAppender = userIDs.reduce((acc, id) => {
            if (isUserDetail) {
                acc += "id=" + id + "&";
            }
            else {
                acc += "userId=" + id + "&";
            }
            return acc;
        }, "?");
        resolve(urlAppender);
    })
}

const userExtraction = [2, 4, 3, 6, 7, 1];

Promise.all(userExtraction)
    .then(value => urlAppend(value, true))
    .then(value => fetchingAPIData("https://jsonplaceholder.typicode.com/users", value))
    .then(value => console.log(value));
Promise.all(userExtraction)
    .then(value => urlAppend(value, false))
    .then(value => fetchingAPIData("https://jsonplaceholder.typicode.com/todos", value))
    .then(value => console.log(value));


/* 
Promise.all([urlAppend(userExtraction)]).then(value => fetchingAPIData("https://jsonplaceholder.typicode.com/users",value)).then(value => console.log(value))

Promise.all([urlAppend(userExtraction,false)]).then(value => fetchingAPIData("https://jsonplaceholder.typicode.com/todos",value)).then(value => console.log(value))
*/



