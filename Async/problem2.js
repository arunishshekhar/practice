/* 
Q2. Write a method that returns a promise to read a File (use fs.readFile inside). 
        your method readFile should use fs.readFile and return a promise.
        your method readFile should accept a file path.
        your method should throw an error with message saying "Missing File." if path is incorrect.
        your method should also accept another optional params for transformation of the data .        
       
        write a method for Transformation of data after read operation 
        ...in the form [solutionA, solutionB, solutionC, solutionD]. 
*/


const fs = require('fs');
const path = require('path');
const workingDir = path.join(__dirname);

function readingfile(filename, transformation) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(workingDir, filename), (err, data) => {
            if (err) {
                reject("Failed to read " + filename + " Missing file : " + err);
            }
            else {
                let stringData = data.toString();
                let returningArray = [];
                transformation.forEach((element) => {
                    returningArray.push(element(stringData))
                })
                resolve(returningArray);
            }
        })
    });
}

function toUppar(data) {
    return data.toUpperCase();
}

function toLower(data) {
    return data.toLowerCase();
}

function length(data) {
    return data.length;
}
async function solver () {
    const output = await readingfile("sample.txt",[toUppar, toLower,length,(data)=>{return data.split(" ")}]);
    console.log(output);
}

solver();

