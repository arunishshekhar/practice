/*

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos
Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913&id=2399
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392&userId=213921398


Q1.Write a function to fetch list of all todos from the above url using both fetch and axios.get.
    	
    A. use promises 
    B. use await keyword 	
	
    C. Once the list is fetched. Group the list of tasks based on user IDs.
        Make sure the non completed tasks are always in front.
    { 
        userId1: [ // All the tasks of userId1]
            ,
        userId2: [ // All the tasks of userId2]

        ...
    
    D.  Also Group tasks based on completed or nonCompleted for each user
        { 
          completed: [..All the completed tasks],
          nonCompleted: [...All the non completed tasks]
        }

    E. Dump the results in a file.

        {
            A: solutionA,
            B: solutionB,
            c: solutionC,
            d: solutionD
        }
*/
const fs = require('fs');
const path = require('path');
const axios = require('axios');

const workingDir = path.join(__dirname);

function writingfile(filename, dataToWrite) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(workingDir, filename), dataToWrite, (err) => {
            if (err) {
                reject("Failed to write" + filename + ": " + err);
            }
            else {
                resolve();
            }
        })
    });
}

function readingfile(filename) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(workingDir, filename), (err, data) => {
            if (err) {
                reject("Failed to read " + filename + ": " + err);
            }
            else {
                let stringData = data.toString();
                resolve(stringData);
            }
        })
    });
}

function fetchingAPIData(url) {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then(response => resolve(response.data))
            .catch(err => reject("Failed to fetch " + url + ": " + err));
    })

}

//Problem A
function problem1(url) {
    fetchingAPIData(url)
        .then(response =>
            writingfile("responseA.json", JSON.stringify(response))
        );
}


//Problem B
async function problem2(url) {
    const dataFromURL = await fetchingAPIData(url);
    writingfile("responseB.json", JSON.stringify(dataFromURL));
}
//problem2();


//Problem C
async function problem3() {
    const dataFromFile = await readingfile("responseB.json");
    let jsonData = await JSON.parse(dataFromFile);
    let returningArray = {};
    jsonData.forEach(response => {
        if (!returningArray[response['userId']]) {
            returningArray[response['userId']] = [];
        }
        returningArray[response['userId']].push(response);
    });
    for (let indivisual in returningArray) {
        returningArray[indivisual].sort((a, b) => {
            if (a['completed'] === false)
                return -1;
        });
    }
    await writingfile('responseC.json', JSON.stringify(returningArray));
}

//problem3();

//Problem D

async function problem4() {
    const dataFromFile = await readingfile("responseA.json");
    let jsonData = await JSON.parse(dataFromFile);
    let returningArray = {};
    jsonData.forEach(response => {
        if (!returningArray[response['completed']]) {
            returningArray[response['completed']] = [];
        }
        returningArray[response['completed']].push(response);
    });
    await writingfile('responseD.json', JSON.stringify(returningArray));
}

//problem4();

//Problem E
async function finalProblem() {
    const url = "https://jsonplaceholder.typicode.com/todos";
    problem1(url);
    await problem2(url);
    await problem3();
    await problem4();
}

finalProblem();