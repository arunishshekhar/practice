/*
    Problem 2:
    
    Using promise and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. 
            Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. 
            Then split the contents into sentences. Then write it to a new file. 
            Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. 
            Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all 
            the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

const workingDir = path.join(__dirname);

function readingfile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(workingDir, fileName), (err, data) => {
            if (err) {
                reject("Error while reading File ", fileName);
            }
            else {
                let dataString = data.toString();
                resolve(dataString);
            }
        });
    });
}

function writingfile(fileName, dataToWrite) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(workingDir, fileName), dataToWrite, (err) => {
            if (err) {
                reject("Error while writing File ", fileName);
            }
            else {
                resolve(fileName);
            }
        });
    });
}

function deleteFiles(value) {
    return new Promise((_,reject) => {
            fileList = value.toString().trim().split("\n")
            fileList.forEach(element => {
                fs.unlink(path.join(workingDir, element), (err) => {
                    if (err) {
                        reject("Error while deleting File ", element);
                    }
                })
            });
    }).then(results => console.log(results));
}

function appendingfile(fileName, dataToWrite) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(workingDir, fileName), dataToWrite + '\n', (err) => {
            if (err) {
                reject("Error while appending File ", dataToWrite);
            }
            else {
                resolve(dataToWrite);
            }
        });
    });
}

module.exports = {readingfile,writingfile,deleteFiles,appendingfile}

