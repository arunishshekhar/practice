const { readingfile, writingfile, deleteFiles, appendingfile } = require('../problem2')

readingfile("lipsum.txt")
    .then(value => {
        return writingfile("uppercase.txt", value.toUpperCase())
    })
    .then(value => {
        return appendingfile("filename.txt", value)
    })
    .then(value => {
        return readingfile(value)
    })
    .then(value => {
        let tempData = value.toLowerCase().split(".").map((eachLine) => { return eachLine.trim() }).join('\n').trim();
        return writingfile('lowercase.txt', tempData)
    })
    .then(value => {
        return appendingfile("filename.txt", value)
    })
    .then(value => {
        return readingfile(value)
    })
    .then(value => {
        let tempData = value.split("\n").sort().join("\n");
        return writingfile('sortedlower.txt', tempData)
    })
    .then(value => {
        appendingfile("filename.txt", value)
    })
    .then(() => {
        return readingfile("filename.txt");
    })
    .then(value => {
        deleteFiles(value);
    })
    .catch(reason => { console.log(reason) });