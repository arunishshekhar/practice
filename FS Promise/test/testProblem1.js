
const {directoryChecker,jsonCreator,directoryDeleator} = require('../problem1')

let dict = {
    "one": [15, 4.5],
    "two": [34, 3.3],
    "three": [67, 5.0],
    "four": [32, 4.1]
};

let dictstring = JSON.stringify(dict);

directoryChecker("DataStorage")
    .then(value => jsonCreator(value, dictstring))
    .then(value => jsonCreator(value, dictstring))
    .then(value => jsonCreator(value, dictstring))
    .then(value => jsonCreator(value, dictstring))
    .then(value => jsonCreator(value, dictstring))
    .then(value => jsonCreator(value, dictstring))
    .then(value => directoryDeleator(value))
    .catch(reason => console.log(reason));