/* 
Note
-Log each operation that you do.
-Try to minimize number of api calls if possible.
-Try to avoid waiting for log promise to finish.
-Sign in and getBooks Promise rejections must always give appropriate responseCode and reason.
    reason: string     example - Auth Failure  or Service failure
    responseCode: number -  example: 401 or 500
-Activity Logs should be stored in an array.


Q. Write a signIn function
Function signature: takes a single argument username and returns a promise.
Returned Promise's value is based on seconds (getSeconds) from current date.
only resolve the promise if seconds are greater than 30 

Q. Write a getBooks method that returns a promise
it picks a random integer between [0 and 1].
if integer picked is 1 ..it resolves the promise with list of Booksstring
else rejects the promise saying service error.


Q. Execute sign In 
Once Signin is successful, make getBooks call.
Only make getBooks call if you are signed in.

Log the results for all possible operations.
Sign In - Success,
Sign In - Failure,
GetBooks - Success,
GetBooks - Failure.

Q. Write a logData method that always returns a resolved promise. It takes 1 second to finish its operation.
It Accepts the activity and saves the activity logs. 

Q. Write a function to display all activity logs.
Signature: accepts no Params.
Return:  name of the user that calls it and all its logs. 

Q. Perform sign in and getBooks calls for 2 Users (Mary, Emily)

Q. Finally display the logs for both of them.

*/

const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]

let usersArray = [];
class Users {
    constructor(name) {
        this.userName = name;
    }
    isLoggedIn = false;
    activity = [];
}

let getDateAndTime = {
    now: new Date,
    seconds: () => getDateAndTime.now.getSeconds(),
    minutes: () => getDateAndTime.now.getMinutes(),
    hour: () => getDateAndTime.now.getHours(),
    date: () => getDateAndTime.now.getUTCDate(),
    month: () => getDateAndTime.now.getUTCMonth() + 1,
    year: () => getDateAndTime.now.getUTCFullYear(),
    milisec: () => getDateAndTime.now.getUTCMilliseconds(),
    totalDate: () => `${getDateAndTime.date()}/${getDateAndTime.month()}/${getDateAndTime.year()}`,
    totalTime: () => `${getDateAndTime.hour()}:${getDateAndTime.minutes()}:${getDateAndTime.seconds()}`
}


function loggingInformation(params = {
    userName: "undefined",
    responseCode: 400,
    additionalInfo: "No additional Information was added"
}) {
    return new Promise((resolve, reject) => {
        let message = '';
        switch (params.responseCode) {
            case 200:
                message = 'Success'
                break;
            case 400:
                message = 'Bad Request'
                break;
            case 500:
                message = 'Bad Gateway'
                break;
            default:
                message = 'Unknown Error'
                break;
        }
        let informationToLog = {
            date: getDateAndTime.totalDate(),
            time: getDateAndTime.totalTime(),
            userName: params.userName,
            responseCode: params.responseCode,
            message: message,
            additionalInfo: params.additionalInfo
        }
    })
}

function signIn(user) {
    return new Promise((resolve, reject) => {
        let userClass = new Users(user)
        

        if (getDateAndTime.seconds() > 0) {
            user.isLoggedIn = true;
            const dataToSend = { userName: user, responseCode: 200, additionalInfo: "Successfully Logged in." }
            loggingInformation(dataToSend);
            resolve(dataToSend);
        }
        else {
            const dataToSend = {userName: user, responseCode: 400, additionalInfo: "Cannot Log in." }
            loggingInformation(dataToSend);
            reject(dataToSend);
        }
    })
}

function getBooks(params) {
    const randomNumber = Math.round(Math.random() * 1);
    return new Promise((resolve, reject) => {
        if (randomNumber === 1 && userDetails.find(element => element.isSignedIn === true)) {
            const dataToSend = { userName: params.userName, responseCode: 200, additionalInfo: "Allowed to Search for book." };
            loggingInformation(dataToSend);
            resolve(Books);
        }
        else {
            const dataToSend = { userName: params.userName, responseCode: 500, additionalInfo: "Service Error." };
            loggingInformation(dataToSend);
            reject(dataToSend);
        }
    })
}

function getIndivisualLog(params) {
    return new Promise((resolve, reject) => {
        userDetails.forEach((user) => {
            if (user.userName === params.userName && user.isSignedIn) {
                resolve(user.individualLog);
            }
            reject("Log not found");
        })
    })
}



/* const users = ['mary', 'Mary', 'Emily']
async function problemSolver(user) {

    try {
        let para = await signIn(user);
        let response = await getBooks(para);
        let output = await getIndivisualLog(para);
        console.log(response);
        console.log(output)
    } catch (err) {
        console.log(err);
    }

}

users.forEach( element => problemSolver(element));
 */

/* 
async function problemSolverTest() {
    let para = {
        userName: "monica",
        individualLog: [],
        isSignedIn: false

    };
    try {
        let response = await getBooks(para);
        console.log(response);
        let output = await getIndivisualLog(para);
        console.log(output);
    } catch (err) {
        console.log(err);
    }
}
problemSolverTest();
 */