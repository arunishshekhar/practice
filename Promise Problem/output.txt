[
  { name: 'Hostilities of War', _id: 'book293492178' },
  { name: 'A Beautiful Sunset', _id: 'book293492178' },
  { name: 'Lorem Ipsum', _id: 'book293492178' },
  { name: 'Rogue Asassin', _id: 'book293492178' }
]
[
  { name: 'Hostilities of War', _id: 'book293492178' },
  { name: 'A Beautiful Sunset', _id: 'book293492178' },
  { name: 'Lorem Ipsum', _id: 'book293492178' },
  { name: 'Rogue Asassin', _id: 'book293492178' }
]
[
  { name: 'Hostilities of War', _id: 'book293492178' },
  { name: 'A Beautiful Sunset', _id: 'book293492178' },
  { name: 'Lorem Ipsum', _id: 'book293492178' },
  { name: 'Rogue Asassin', _id: 'book293492178' }
]
{
  userName: 'Mary',
  responseCode: 500,
  additionalInfo: 'Service Error.'
}
{
  userName: 'Full Metal Alcamist Brotherhood',
  responseCode: 500,
  additionalInfo: 'Service Error.'
}
Log for Mary
[
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Mary',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Successfully Logged in.'
  },
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Mary',
    responseCode: 500,
    message: 'Bad Gateway',
    additionalInfo: 'Service Error.'
  }
]
Log for Emily
[
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Emily',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Successfully Logged in.'
  },
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Emily',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Allowed to Search for book.'
  }
]
Log for doremon
[
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'doremon',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Successfully Logged in.'
  },
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'doremon',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Allowed to Search for book.'
  }
]
Log for Naruto
[
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Naruto',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Successfully Logged in.'
  },
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Naruto',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Allowed to Search for book.'
  }
]
Log for Full Metal Alcamist Brotherhood
[
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Full Metal Alcamist Brotherhood',
    responseCode: 200,
    message: 'Success',
    additionalInfo: 'Successfully Logged in.'
  },
  {
    date: '13/4/2022',
    time: '10:13:56',
    userName: 'Full Metal Alcamist Brotherhood',
    responseCode: 500,
    message: 'Bad Gateway',
    additionalInfo: 'Service Error.'
  }
]
