let getDateAndTime = {
    now: new Date,
    seconds: () => getDateAndTime.now.getSeconds(),
    minutes: () => getDateAndTime.now.getMinutes(),
    hour: () => getDateAndTime.now.getHours(),
    date: () => getDateAndTime.now.getUTCDate(),
    month: () => getDateAndTime.now.getUTCMonth() + 1,
    year: () => getDateAndTime.now.getUTCFullYear(),
    milisec: () => getDateAndTime.now.getUTCMilliseconds(),
    totalDate: () => `${getDateAndTime.date()}/${getDateAndTime.month()}/${getDateAndTime.year()}`,
    totalTime: () => `${getDateAndTime.hour()}:${getDateAndTime.minutes()}:${getDateAndTime.seconds()}`
}

const Books = [{
    name: "Hostilities of War",
    _id: "book293492178",
}, {
    name: "A Beautiful Sunset",
    _id: "book293492178",
}, {
    name: "Lorem Ipsum",
    _id: "book293492178",
}, {
    name: "Rogue Asassin",
    _id: "book293492178",
}]
let usersArray = {};
let userList = [];
class IndividualUser {
    constructor(name) {
        this.userName = name;
    }
    isLoggedIn = false;
    activity = [];
    showLog = () => {
        if (this.isLoggedIn) {
            console.log("Log for " + this.userName)
            return this.activity;
        }
    }
}

function createUser(username) {
    usersArray[username] = new IndividualUser(username);
    userList.push(username);
}

function loggingInformation(params) {
    return new Promise((resolve, reject) => {
        let message = '';
        switch (params.responseCode) {
            case 200:
                message = 'Success'
                break;
            case 400:
                message = 'Bad Request'
                break;
            case 500:
                message = 'Bad Gateway'
                break;
            default:
                message = 'Unknown Error'
                break;
        }
        let informationToLog = {
            date: getDateAndTime.totalDate(),
            time: getDateAndTime.totalTime(),
            userName: params.userName,
            responseCode: params.responseCode,
            message: message,
            additionalInfo: params.additionalInfo
        }
        usersArray[params.userName].activity.push(informationToLog);
        resolve(usersArray[params.userName])
    })
}

function signIn(user) {
    return new Promise((resolve, reject) => {
        let userDetail = usersArray[user];
        if (userDetail && getDateAndTime.seconds() > 0) {
            userDetail.isLoggedIn = true;
            const dataToSend = { ...userDetail, responseCode: 200, additionalInfo: "Successfully Logged in." }
            loggingInformation(dataToSend);
            resolve(usersArray[user]);
        }
        else {
            const dataToSend = { userName: userDetail.userName, responseCode: 400, additionalInfo: "Cannot Log in." }
            loggingInformation(dataToSend);
            reject(dataToSend);
        }
    })
}

function getBooks(params) {
    const randomNumber = Math.round(Math.random() * 1);
    return new Promise((resolve, reject) => {
        if (randomNumber === 1 && params.isLoggedIn === true) {
            const dataToSend = { ...params, responseCode: 200, additionalInfo: "Allowed to Search for book." };
            loggingInformation(dataToSend);
            resolve(Books);
        }
        else {
            const dataToSend = { userName: params.userName, responseCode: 500, additionalInfo: "Service Error." };
            loggingInformation(dataToSend);
            reject(dataToSend);
        }
    })
}


/* async function indivisualUserAction(user) {
    const signInOutput = await signIn(user);
    const getBooksOutput = await getBooks(user);
    console.log(getBooksOutput);
    console.log(usersArray[user].showLog())
}
 */
function problemSolver() {
    createUser('Mary');
    createUser('Emily');
    createUser('doremon');
    createUser('Naruto');
    createUser('Full Metal Alcamist Brotherhood');
    try {
        userList.forEach(user => {
            //indivisualUserAction(user);
            signIn(user)
                .then(value => getBooks(value))
                .then(value => console.log(value))
                .catch(err => console.log(err))
                .finally(() => console.log(usersArray[user].showLog()));
            /*  signIn('Emily')
                 .then(value => getBooks(value))
                 .then(value => console.log(value))
                 .catch(err => console.log(err))
                 .finally(() => console.log(usersArray['Emily'].showLog())); */

        })

    } catch (err) {
        console.log(err);
    }

}
problemSolver();
