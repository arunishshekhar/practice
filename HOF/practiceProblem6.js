/*
6. Find the average salary of based on country using only HOF method 
*/

const fs = require('fs');
const workingDir = __dirname + '/'

const readFile = (callback) => {
    return new Promise((resolve, reject) => {
        fs.readFile((workingDir + 'Practise_Problems.json'), (err, data) => {
            if (err) {
                reject("error while reading file" + err);
            }
            else {
                const dataString = data.toString();
                let jsonData = JSON.parse(dataString);
                const returnedValue = callback(jsonData);
                resolve(returnedValue);
            }
        });
    })
}

const modifyingSalary = (information) => {
    return information.map(data => {
        let salaried = data.salary.replace('$', '') * 1;
        data["corrected_salary"] = salaried * 10000;
        return data;
    });

};

async function groupingDataByCountry() {
    const someData = await readFile(modifyingSalary);
    let groupedData = [];
    someData.forEach(data => {
        if (!groupedData[data['location']])
            groupedData[data['location']] = [];
        groupedData[data['location']].push(data);
    })
    return groupedData;
}

groupingDataByCountry()
    .then(response => {
        returningObject = {};
        for (const [key, value] of Object.entries(response)) {
            let tempSal = value.reduce((acc,data) => acc+data.corrected_salary,0)
            let avgSal = tempSal/value.length;
            returningObject[key.toString()] = avgSal;
          }
        console.log(returningObject)
    })