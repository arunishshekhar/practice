

/* 
7. Calculate how many .org, .au, .com emails are there
*/
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const emailCounter = (information) => {
    return information.filter(data => {
        let splittedData = data.email.split('.');
        return splittedData[splittedData.length -1 ] === 'org' || splittedData[splittedData.length -1] === 'au' || splittedData[splittedData.length -1] === 'com';
    }).length;
};
readFile(emailCounter);