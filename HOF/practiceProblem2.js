/* 
2.Convert all the salary values into proper numbers instead of strings
*/
/*
3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
4. Find the sum of all salaries 
5. Find the sum of all salaries based on country using only HOF method
6. Find the average salary of based on country using only HOF method 
*/

const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Practise_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const salariesStringToNumber = (information) => {
    return information.map(data => {
        let salaried = data.salary.replace('$', '') * 1;
        data.salary = salaried;
        return data;
    });

};

readFile(salariesStringToNumber);