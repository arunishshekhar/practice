

/* 
6. Filter out all the .org emails
*/
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const filterORG = (information) => {
    return information.filter(data => {
        let splittedData = data.email.split('.');
        return splittedData[splittedData.length -1 ] === 'org';
    });
};
readFile(filterORG);