

/* 
4. Find the sum of all the fourth components of the ip addresses.
*/
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const ip4thValueAdder = (information) => {
    let initialVal = 0;
    return information.reduce((acc, data) => {
        let splittedData = data.ip_address.split('.');
        return acc + (splittedData[3]*1);
    },initialVal);
};
readFile(ip4thValueAdder);