
/* 
8. Sort the data in descending order of first name
*/
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const sortingByFirstName = (information) => {
    let sortedName = information.sort((element1, element2) => {
        if (element1.first_name < element2.first_name) {
            return 1;
        }
    });
    return sortedName;
};
readFile(sortingByFirstName);