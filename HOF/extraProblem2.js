

/* 
2. Split their IP address into their components eg. 111.139.161.143 has components [
    111,
    139,
    161,
    143
]
 */
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const ipSpliier = (information) => {
    let initialVal = [];
    return information.reduce((acc, data) => {
        let splittedData = data.ip_address.split('.');
        acc.push(splittedData);
        return acc;
    },initialVal);
};
readFile(ipSpliier);