/* 
1. Find all Web Developers
*/

const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Practise_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            console.log(callback(jsonData));
        }
    });
}

const findWebDeveloper = (information) => {
    return information.filter(individual => individual.job.startsWith('Web Developer'))
};

readFile(findWebDeveloper);