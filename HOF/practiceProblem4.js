/*
4. Find the sum of all salaries 
*/

const fs = require('fs');
const workingDir = __dirname + '/'

const readFile = (callback) => {
    return new Promise((resolve, reject) => {
        fs.readFile((workingDir + 'Practise_Problems.json'), (err, data) => {
            if (err) {
                reject("error while reading file" + err);
            }
            else {
                const dataString = data.toString();
                let jsonData = JSON.parse(dataString);
                const returnedValue = callback(jsonData);
                resolve(returnedValue);
            }
        });
    })
}

const modifyingSalary = (information) => {
    return information.map(data => {
        let salaried = data.salary.replace('$', '') * 1;
        data["corrected_salary"] = salaried * 10000;
        return data;
    });

};

async function problemSolver() {
    const someData = await readFile(modifyingSalary);
    const totalSalary = someData.reduce((acc,curr) => acc + curr.corrected_salary,0);
    console.log(totalSalary);
}

problemSolver();