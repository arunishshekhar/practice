/*
3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
*/

const fs = require('fs');
const workingDir = __dirname + '/'

const readFile = (callback) => {
    return new Promise((resolve, reject) => {
        fs.readFile((workingDir + 'Practise_Problems.json'), (err, data) => {
            if (err) {
                reject("error while reading file" + err);
            }
            else {
                const dataString = data.toString();
                let jsonData = JSON.parse(dataString);
                const returnedValue = callback(jsonData);
                resolve(returnedValue);
            }
        });
    })
}

const modifyingSalary = (information) => {
    return information.map(data => {
        let salaried = data.salary.replace('$', '') * 1;
        data["corrected_salary"] = salaried * 10000;
        return data;
    });

};

async function problemSolver() {
    const someData = await readFile(modifyingSalary);
    console.log(someData);
}

problemSolver();