

/* 
5. Compute the full name of each person and store it in a new key (full_name or something) for each person.
*/
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            let jsonData = JSON.parse(dataString);
            callback(jsonData);
        }
    });
}

const fullNameAdder = (information) => {
    information.forEach((key) => {
        let tempName = key.first_name + ' ' + key.last_name;
        key['full_name'] = tempName;
    });

    console.log(information);
};
readFile(fullNameAdder);