

/* 1. Find all people who are Agender
 */
const fs = require('fs');
const workingDir = __dirname + '/'
const readFile = (callback) => {
    fs.readFile((workingDir + 'Extra_Problems.json'), (err, data) => {
        if (err) {
            throw err;
        }
        else {
            const dataString = data.toString();
            //console.log(dataString);
            let jsonData = JSON.parse(dataString);
            //console.log(typeof jsonData);
            console.log(callback(jsonData));
        }
    });
}

const peopleAgender = (information) => {
    //console.log(typeof information)
    return information.filter(person => {
        return (person.gender === "Agender")
    });
};
readFile(peopleAgender);